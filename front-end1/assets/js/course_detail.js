$(document).ready(function () {
    var gUrlString = window.location.href;
    var gNewUrl = new URL(gUrlString);
    var gCourseId = gNewUrl.searchParams.get("id");
    var gCourseObj = {
        id: "",
        courseName: "",
        courseDescription: "",
        courseImgUrl: "",
        coursePrice: 0,
    }
    var gVote = 0;
    var gUserRole = "";

    //on page load
    getCourseInfoById();

    //get product info by id
    function getCourseInfoById() {

        $.ajax({
            url: "http://localhost:8080/course?id=" + gCourseId,
            type: "GET",
            success: function (pCourseObj) {
                gCourseObj = pCourseObj;
                loadCourseData();
            },
            error: function (ajaxContext) {

            }
        })
    }

    //load Course Detail Data
    function loadCourseData() {
        $("#courseName").text(gCourseObj.courseName);
        $("#courseImg").attr("src", gCourseObj.courseImgUrl);
        $("#courseDescription").text(gCourseObj.courseDescription);
        $("#coursePrice").text(gCourseObj.coursePrice.toLocaleString('vi', { style: 'currency', currency: 'VND' }));
        checkShowContentForLoginUser();
    }

    //action button add course to cart
    $("#addToCartBtn").on("click", function (event) {
        event.preventDefault();
        var vCourse = {
            id: gCourseObj.id,
            courseName: gCourseObj.courseName,
            coursePrice: gCourseObj.coursePrice,
            courseImgUrl: gCourseObj.courseImgUrl
        }
        saveCourse(vCourse);
    })

    //===========PAGE COURSE DETAIL============
    //show lesson content for paid user
    function checkShowContentForLoginUser(){
        var vUserName = $("#userInfo").data("userid");
        console.log("user data:" + vUserName);
    }

    //get Local Storage
    function getLSContent() {
        // get contents from local storage.
        // if nothing is there, create an empty array
        const lsContent = JSON.parse(localStorage.getItem("courses")) || [];
        return lsContent;
    }

    function setLSContent(lsContent) {
        // save content inside local storage
        localStorage.setItem("courses", JSON.stringify(lsContent));
    }

    function saveCourse(pCourseObj) {
        // save selected product in local storage and display it in the cart together

        // vars
        var vCoursetId = pCourseObj.id;
        var vCourseName = pCourseObj.courseName;
        var vCoursePrice = pCourseObj.coursePrice;
        var vCourseImgUrl = pCourseObj.courseImgUrl;
        let isCourseInCart = false;

        // get local storage array
        const lsContent = getLSContent();

        // to avoid user adds the same course twice, check
        // the product is not in LS already before adding it
        lsContent.forEach(function (course) {
            if (course.id === vCoursetId) {
                isCourseInCart = true;
                //thông báo đã có sản phẩm trong giỏ hàng
                callToast("duplicate");
            }
        });

        // only if the product is not already in the cart,
        // create an object representing selected product info
        // and push it into local storage array
        if (!isCourseInCart) {
            lsContent.push({
                id: vCoursetId,
                name: vCourseName,
                price: vCoursePrice,
                imgUrl: vCourseImgUrl
            });

            // add product into into local storage
            setLSContent(lsContent);
            callToast("success");
        }
    }

    //Call toast
    function callToast(pToastType) {
        if (pToastType == "success") {
            toastr.success('Đã thêm khoá học vào giỏ');
        }
        if (pToastType == "duplicate") {
            toastr.info('Khoá học đã có trong giỏ');
        }

    }
})