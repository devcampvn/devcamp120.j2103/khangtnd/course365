$(document).ready(function () {
    //global varible
    gGetAllCourseAPI = "http://localhost:8080/courses";
    gPageObject = {
        totalItems: 0,
        totalPages: 0,
        currentPage: 0,
        courses: []
    }
    gCurrentPageSelect = 0;
    gListCourseAll = true;
    gProductSize = 6;
    gWebCourseTypeId = 1;
    gMobileCourseTypeId = 2;
    gBlockchainCourseTypeId = 3;

    //function on loading page
    getCourseData();

    function getCourseData() {
        $.ajax({
            url: gGetAllCourseAPI,
            type: "GET",
            success: function (pCourseObj) {
                gPageObject = pCourseObj;
                loadCourseData();
            },
        })
    }

    function loadCourseData() {
        var vCourseSection = $("#courseList");
        vCourseSection.text("");

        for (i = 0; i < gPageObject.courses.length; i++) {
            var bCourse = $(`<div class="col-lg-4">
                                <div class="properties properties2 mb-30">
                                    <div class="properties__card">
                                        <div class="properties__img overlay1">
                                            <a href="course-detail.html?id=${gPageObject.courses[i].id}"><img src="${gPageObject.courses[i].courseImgUrl}" style="max-height: fit-content;max-width: fit-content;"></a>
                                        </div>
                                        <div class="properties__caption">
                                            <p>User Experience</p>
                                            <h3><a href="course-detail.html?id=${gPageObject.courses[i].id}">${gPageObject.courses[i].courseName}</a></h3>
                                            <p>${gPageObject.courses[i].courseDescription}
                                            </p>
                                            <div class="properties__footer d-flex justify-content-between align-items-center">
                                                <div class="restaurant-name">
                                                    <div class="rating">
                                                        <i class="fas fa-star"></i>
                                                        <i class="fas fa-star"></i>
                                                        <i class="fas fa-star"></i>
                                                        <i class="fas fa-star"></i>
                                                        <i class="fas fa-star-half"></i>
                                                    </div>
                                                    <p><span>(4.5)</span>voted by 120 user</p>
                                                </div>
                                                <div class="price">
                                                    <span>${gPageObject.courses[i].coursePrice.toLocaleString('vi', { style: 'currency', currency: 'VND' })}</span>
                                                </div>
                                            </div>
                                            <a class="border-btn border-btn2" id="addCourseBtn"
                                            data-course_id=${gPageObject.courses[i].id}
                                            data-course_name="${gPageObject.courses[i].courseName}"
                                            data-course_price=${gPageObject.courses[i].coursePrice}
                                            data-img_url=${gPageObject.courses[i].courseImgUrl} >Thêm Vào Giỏ</a>
                                        </div>
                                    </div>
                                </div>
                            </div>         
                        `)
            $("#courseList").append(bCourse);
        }

        pagination();
    }

    //phân trang
    function pagination() {
        var vPageArea = $("#pageNumber");
        vPageArea.text("");
        var prePage = `<a class="genric-btn primary-border radius" id="previousPage"><i class="fas fa-chevron-left"></i></a>`;
        var nextPage = `<a class="genric-btn primary-border radius" id="nextPage"><i class="fas fa-chevron-right"></i></a>`;
        vPageArea.append(prePage);
        for (i = 0; i < gPageObject.totalPages; i++) {
            var pageNumber = `<a class="genric-btn primary-border radius page-number" data-current_page="${i + 1}">${i + 1}</a>`
            vPageArea.append(pageNumber);
        }
        vPageArea.append(nextPage);
    }

    $("#pageNumber").on("click", ".page-number", function () {
        //phải trừ 1 vì index của page bắt đầu  = 0
        var currentPage = $(this).text() - 1;
        gCurrentPageSelect = currentPage;
        console.log("Current page: " + currentPage);
        if (gListCourseAll == true) {
            getCourseBySelectPage();
        }

    })

    //sự kiện previous page
    $("#pageNumber").on("click", "#previousPage", function () {
        if (gPageObject.currentPage == 0) {
            console.log("Đang ở trang đầu tiên")
        } else {
            gCurrentPageSelect = gPageObject.currentPage - 1;
            if (gListCourseAll == true) {
                getCourseBySelectPage();
            }
        }
    })

    //sự kiện next page
    $("#pageNumber").on("click", "#nextPage", function () {
        if (gPageObject.currentPage + 1 == gPageObject.totalPages) {
            console.log("Đang ở trang cuối cùng")
        } else {
            gCurrentPageSelect = gPageObject.currentPage + 1;
            if (gListCourseAll == true) {
                getCourseBySelectPage();
            }
        }
    })

    //get course data by id
    function getCourseBySelectPage() {
        $.ajax({
            url: "http://localhost:8080/courses?page=" + gCurrentPageSelect + "&size=" + gProductSize,
            type: "GET",
            success: function (pObj) {
                console.log("Response Text:", pObj);
                gPageObject = pObj;
                loadCourseData();
            },
        })
    }


    //action button add course to cart
    $("#courseList").on("click", "#addCourseBtn", function () {
        var vCourse = {
            id: $(this).data("course_id"),
            courseName: $(this).data("course_name"),
            coursePrice: $(this).data("course_price"),
            courseImgUrl: $(this).data("img_url")
        }
        saveCourse(vCourse);
    })

    //===============action filter button============
    //action mobile course type
    $("#mobileCourseType").on("click", function () {
        filterCourse(gMobileCourseTypeId);
    })

    //action web course type
    $("#webCourseType").on("click", function () {
        filterCourse(gWebCourseTypeId);
    })

    //action blockchain course type
    $("#blockchainCourseType").on("click", function () {
        filterCourse(gBlockchainCourseTypeId);
    })

    $("#allCourseType").on("click", function () {
        getCourseData();
    })

    //call filter course by type id
    function filterCourse(pCourseTypeId) {
        $.ajax({
            url: "http://localhost:8080/course/filter?type=" + pCourseTypeId,
            type: "GET",
            success: function (pObj) {
                gPageObject = pObj;
                loadCourseData();
            },
        })
    }

    //get Local Storage
    function getLSContent() {
        // get contents from local storage.
        // if nothing is there, create an empty array
        const lsContent = JSON.parse(localStorage.getItem("courses")) || [];
        return lsContent;
    }

    function setLSContent(lsContent) {
        // save content inside local storage
        localStorage.setItem("courses", JSON.stringify(lsContent));
    }

    function saveCourse(pCourseObj) {
        // save selected product in local storage and display it in the cart together

        // vars
        var vCoursetId = pCourseObj.id;
        var vCourseName = pCourseObj.courseName;
        var vCoursePrice = pCourseObj.coursePrice;
        var vCourseImgUrl = pCourseObj.courseImgUrl;
        let isCourseInCart = false;

        // get local storage array
        const lsContent = getLSContent();

        // to avoid user adds the same course twice, check
        // the product is not in LS already before adding it
        lsContent.forEach(function (course) {
            if (course.id === vCoursetId) {
                isCourseInCart = true;
                //thông báo đã có sản phẩm trong giỏ hàng
                callToast("duplicate");
            }
        });

        // only if the product is not already in the cart,
        // create an object representing selected product info
        // and push it into local storage array
        if (!isCourseInCart) {
            lsContent.push({
                id: vCoursetId,
                name: vCourseName,
                price: vCoursePrice,
                imgUrl: vCourseImgUrl
            });

            // add product into into local storage
            setLSContent(lsContent);
            callToast("success");
        }
    }

    //Call toast
    function callToast(pToastType) {
        if (pToastType == "success") {
            toastr.success('Đã thêm khoá học vào giỏ');
        }
        if (pToastType == "duplicate") {
            toastr.info('Khoá học đã có trong giỏ');
        }

    }

})