$(document).ready(function () {
    var gIsExistCustomer = false;
    var gDataSingUp = {
        email: "",
        passWord: "",
        confirmPassWord: "",
    };
    function getDataSingUp() {
        gDataSingUp.email = $("#emailRegister").val();
        gDataSingUp.passWord = $("#passwordRegister").val();
        gDataSingUp.confirmPassWord = $("#confirmPasswordRegister").val();
    }

    //clear field input
    function clearInput() {
        $("#emailRegister").val("");
        $("#passwordRegister").val("");
        $("#confirmPasswordRegister").val("");
        $("#registerError").text("");
    }

    function validateDataSignUp() {
        var vErrorCheck = false;
        try {
            if (gDataSingUp.email === "") throw ("chưa nhập email");
            if (!validateEmail(gDataSingUp.email)) throw ("email không hợp lệ");
            if (gDataSingUp.passWord === "") throw ("chưa nhập password");
            if (gDataSingUp.confirmPassWord === "") throw ("chưa nhập confirm password");
            if (gDataSingUp.confirmPassWord !== gDataSingUp.passWord) throw ("password confirm không giống nhau");
        } catch (error) {
            $("#registerError").html(error);
            vErrorCheck = true;
        }
        return vErrorCheck;

    }

    // Hàm validate email bằng regex
    function validateEmail(email) {
        const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex.test(String(email).toLowerCase());
    }


    $("#registerModalBtn").on("click", function (event) {
        event.preventDefault();
        getDataSingUp();
        signUpFunction()
    })

    //create user for new or old customer
    function signUpFunction() {
        var vSignUpData = {
            username: gDataSingUp.email,
            password: gDataSingUp.passWord
        }
        var vErrorInputCheck = validateDataSignUp();

        if (vErrorInputCheck == false) {
            // tạo ra tài khoản mới
            $.ajax({
                url: "http://localhost:8080/register",
                type: 'POST',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(vSignUpData),
                success: function (pUserRes) {
                    callToast("success","Đã tạo tài khoản thành công");
                    clearInput();
                },
                error: function (pAjaxContext) {
                    //if user name (email) in user table already exist, then show the error message
                    $("#registerError").html(pAjaxContext.responseText);
                }
            });





        }

    }

    //set role customer for new user
    function setRoleCustomerForUser(pUserId){
        console.log("user id: " + pUserId)
        $.ajax({
            url: "http://localhost:8080/user/" + pUserId + "/roleCustomer",
            type: "POST",
            contentType: "application/json;charset=UTF-8",
            success: function (paramCustomerRes) {
                console.log(paramCustomerRes);
            },
        })
    }

    //Call toast
    function callToast(pToastType,pToastMessage) {
        if (pToastType == "success") {
            toastr.options.newestOnTop = false;
            toastr.success(pToastMessage);
        }
        
    }
})
