$(document).ready(function () {

    //Global varible
    gUserId = 0;

    showItemLSToCart();
    showTotalPrice();

    // get contents from local storage.
    function getLSContent() {

        // if nothing is there, create an empty array
        const vLSContent = JSON.parse(localStorage.getItem("courses"));
        return vLSContent;
    }

    // save content inside local storage
    function setLSContent(pLSContent) {
        localStorage.setItem("courses", JSON.stringify(pLSContent));
    }

    //show item from local storage to cart
    function showItemLSToCart() {
        var vLSContent = getLSContent();
        $("#cartContent > tbody").text("");
        $("#cartContent > tfoot").text("");
        if (vLSContent.length !== 0) {
            for (i = 0; i < vLSContent.length; i++) {
                bCourseMarkup = `<tr class="text-center ">
                                    <td>
                                        ${i + 1}
                                    </td>
                                    <td>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img width="180" height="110" src="${vLSContent[i].imgUrl}">
                                            </div>
                                            <div class="col-md-8">
                                            <a href="course-detail.html?id=${vLSContent[i].id}" style="color:black;font-size:25px">${vLSContent[i].name}</a>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <span class="course-price"style="font-size: 20px">${vLSContent[i].price.toLocaleString('vi', { style: 'currency', currency: 'VND' })}</span>
                                    </td>
                                    <td>
                                    <a title="Xoá khoá học" class="genric-btn danger circle remove" data-course_id=${vLSContent[i].id}><i class="fas fa-minus"></i></a>
                                    </td>
                                </tr>`;
                $("#cartContent").append(bCourseMarkup);
            }
            vCartTableFooter = `<tfoot>
                                    <tr class="text-center">
                                        <td colspan="2" style="font-size:20px;font-weight: bold">Tổng tiền
                                        </td>
                                        <td id="priceTotal" style="font-size:20px;font-weight: bold">
                                        </td>
                                        <td><a class="genric-btn primary-border circle" style="font-size:20px;font-weight: bold" id="checkoutBtn" >Checkout</a>
                                        </td>
                                    </tr>
                                </tfoot>`;
            $("#cartContent").append(vCartTableFooter);
        } else {
            // if no content is in local storage, alert user
            bCourseMarkup = `<tr>
                                <td colspan="6"><p style="font-size: 20px">Bạn chưa chọn khoá học nào</p></td>
                            </tr>`;
            $("#cartContent").append(bCourseMarkup);
        }
    }

    //Show total price
    function showTotalPrice() {
        console.log("Tổng giá tiền là: ");
        var sum = 0;
        // iterate through each td based on class and add the values
        $(".course-price").each(function () {

            //convert string sang float và * thêm 1.000.000 để quy ra số tiền
            var value = parseFloat($(this).text()) * 1000000.00;
            console.log(value)
            // add only if the value is number
            if (!isNaN(value) && value.length != 0) {
                sum += parseFloat(value);
            }
        });
        $("#priceTotal").text(sum.toLocaleString('vi', { style: 'currency', currency: 'VND' }));

    }

    //Hàm xoá sản phẩm khỏi giỏ hàng
    function removeCourse(pCourseId) {
        // remove product from cart (and from local storage)

        // retrieve list of products from LS
        var vLSContent = getLSContent();

        // get the index of the product item to remove
        // inside the local storage content array
        let vCourseIndex;
        vLSContent.forEach(function (courses, i) {
            if (courses.id === pCourseId) {
                vCourseIndex = i;
            }
        });

        // modify the items in local storage array
        // to remove the selected product item

        vLSContent.splice(vCourseIndex, 1);
        // update local storage content
        setLSContent(vLSContent);
        showItemLSToCart();
    }

    //action button remove item from cart
    $("#cartContent").on("click", ".remove", function () {
        console.log("Xoá sản phẩm khỏi cart");
        var vCourseId = $(this).data("course_id")
        removeCourse(vCourseId);
        showTotalPrice();
    })


    //action checkout button
    $("#checkoutBtn").on("click", function () {
        enrollCourse();
    })

    //enroll course for logined user
    function enrollCourse() {
        var vUserName = $("#userInfo").text();
        if (vUserName == "") {
            callToast("warning","Bạn chưa đăng nhập");
        }else{
            console.log("username: " + vUserName);
            findUserIdByUserName(vUserName);
            createPayment();
        }
    }

    //call API find user by username
    function findUserIdByUserName(pUserName) {
        $.ajax({
            url: "http://localhost:8080/user?username=" + pUserName,
            method: "GET",
            success: function (responseObject) {
                gUserId = responseObject.id;
                createPayment(gUserId);
            },
            error: function (xhr) {

            }
        });
    }

    //call create payment API
    function createPayment(){
        var vToday = new Date().format("d-m-Y");
        var vPaymentData = {
            ammount: calTotalPaymentPrice(),
            paymentDate: vToday
        }
        $.ajax({
            url: "http://localhost:8080/payment/" + gUserId,
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(vPaymentData),
            success: function (pPaymentRes) {
                console.log(pPaymentRes);
                enrollCourseUser(pPaymentRes.id)
            },
            error: function (pAjaxContext) {
                
            }
        });
    }

    //calculate payment ammount
    function calTotalPaymentPrice() {
        var lsContent = getLSContent();
        var vPaymentPrice = 0;
        if (lsContent !== null) {
            for (i = 0; i < lsContent.length; i++) {
                var bPriceItem = lsContent[i].price;
                vPaymentPrice += bPriceItem;
            }
        } else {
            return vPaymentPrice;
        }
        return vPaymentPrice;
    }

    //call API enroll course
    function enrollCourseUser(pPaymentId){
        var vCourseInCart = getLSContent();
        for(i = 0; i < vCourseInCart.length; i++){
            $.ajax({
                url: "http://localhost:8080/enroll-course/payment/" + pPaymentId + "/user/" + gUserId + "/course/" + vCourseInCart[i].id +  "/" + "accept",
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                success: function (pEnrollCourseRes) {
                    callToast("success","Bạn đã đăng ký thành công khoá học: " + pEnrollCourseRes.courseId.courseName);
                },
                error: function (pErrorMess){
                    console.log(pErrorMess);
                    callToast("warning", pErrorMess.responseText);
                    deletePaymentById(pPaymentId);
                }
            });
        }
    }

    //call API delete Payment if Course Exist
    function deletePaymentById(pPaymentId){
        $.ajax({
            url: "http://localhost:8080/payment/" + pPaymentId,
            type: 'DELETE',
            success: function (pEnrollCourseRes) {
            },
        });
    }


    //Call toast
    function callToast(pToastType,pToastMessage) {
        if (pToastType == "success") {
            toastr.success(pToastMessage);
        }
        if (pToastType == "duplicate") {
            toastr.info(pToastMessage);
        }
        if (pToastType == "warning") {
            toastr.warning(pToastMessage);
        }

    }


})