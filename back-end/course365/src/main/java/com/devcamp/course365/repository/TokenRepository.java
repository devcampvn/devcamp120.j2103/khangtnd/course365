package com.devcamp.course365.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.course365.model.Token;

public interface TokenRepository extends JpaRepository<Token, Long> {

    Token findByToken(String token);
}
