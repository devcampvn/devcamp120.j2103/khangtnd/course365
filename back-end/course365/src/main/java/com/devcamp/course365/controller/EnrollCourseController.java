package com.devcamp.course365.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.course365.model.CCourse;
import com.devcamp.course365.model.CEnrollCourse;
import com.devcamp.course365.model.CPayment;
import com.devcamp.course365.model.User;
import com.devcamp.course365.repository.ICourseRepository;
import com.devcamp.course365.repository.IEnrollCourse;
import com.devcamp.course365.repository.IPayment;
import com.devcamp.course365.repository.UserRepository;

@CrossOrigin
@RestController
public class EnrollCourseController {
	
	@Autowired
	IEnrollCourse enrollCourseRepository;
	
	@Autowired
	IPayment paymentRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	ICourseRepository courseRepository;
	
	
	
	@GetMapping("/enroll-course/all")
	public List<CEnrollCourse> getAllEnrollCourse(){
		return enrollCourseRepository.findAll();
	}
	
	@GetMapping("/enroll-course")
	public ResponseEntity<?> findCourseByUserId(@RequestParam("userId") Long userId){
		try {
			List<CEnrollCourse> courseFound = enrollCourseRepository.findAllByUserId(userId);
			return new ResponseEntity<>(courseFound, HttpStatus.OK);
		}catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping("/enroll-course/payment/{paymentId}/user/{userId}/course/{courseId}/{courseStatus}")
	public ResponseEntity<Object> createEnrollCourse(
			@PathVariable("paymentId") Integer paymentId,
			@PathVariable("userId") Long userId,
			@PathVariable("courseId") Integer courseId,
			@PathVariable("courseStatus") String courseStatus){
		try {
			Optional<CPayment> paymentFound = paymentRepository.findById(paymentId);
			Optional<User> userFound = userRepository.findById(userId);
			Optional<CCourse> courseFound = courseRepository.findById(courseId);
			List<CEnrollCourse> checkCourseExist = enrollCourseRepository.findAllByUserId(userId);
			Boolean courseExist = false;
			CCourse courseExistFound = null;
			for(int i = 0; i < checkCourseExist.size(); i++ ) {
				if(checkCourseExist.get(i).getCourseId().getId() == courseId) {
					courseExist = true;
					courseExistFound = checkCourseExist.get(i).getCourseId();
				}
			}
			if(courseExist == false) {
				CEnrollCourse newEnrollCourse = new CEnrollCourse();
				newEnrollCourse.setPayment(paymentFound.get());
				newEnrollCourse.setCourseId(courseFound.get());
				newEnrollCourse.setUserId(userFound.get());
				newEnrollCourse.setCourseStatus(courseStatus);
				
				CEnrollCourse createEnrollCourse = enrollCourseRepository.save(newEnrollCourse);
				//List<CEnrollCourse> newEnrollCourse = enrollCourseRepository.addEnrollCourse(courseStatus, courseId, userId, paymentId);
				//List<CEnrollCourse> newEnrollCourse = enrollCourseRepository.addEnrollCourseTest(courseStatus, courseId);
				return new ResponseEntity<>(createEnrollCourse, HttpStatus.OK);
			}else {
				return ResponseEntity.unprocessableEntity().body("Bạn đã đăng ký khoá học: " + courseExistFound.getCourseName());
			}
		}catch(Exception e) {
			return ResponseEntity.unprocessableEntity().body("Lỗi không tạo được payment");
		}
	}
	
}
