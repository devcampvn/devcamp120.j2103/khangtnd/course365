package com.devcamp.course365.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.course365.model.CCourseType;
import com.devcamp.course365.repository.ICourseTypeRepository;

@CrossOrigin
@RestController
public class CourseTypeController {
	
	@Autowired
	ICourseTypeRepository courseTypeRepository;
	
	@GetMapping("/course-type/all")
	public List<CCourseType> getAllCourseType(){
		return courseTypeRepository.findAll();
	}
}
