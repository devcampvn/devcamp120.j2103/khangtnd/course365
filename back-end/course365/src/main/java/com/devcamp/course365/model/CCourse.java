package com.devcamp.course365.model;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="courses")
public class CCourse {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "course_name")
	private String courseName;
	
	@Column(name = "course_description")
	private String courseDescription;
	
	@Column(name = "course_img_url")
	private String courseImgUrl;
	
	@Column(name = "course_price")
	private BigDecimal coursePrice;
	
	@ManyToOne
	@JoinColumn(name ="course_type_id")
	@JsonIgnore
	private CCourseType courseType;
	
	

	/**
	 * @return the coursePrice
	 */
	public BigDecimal getCoursePrice() {
		return coursePrice;
	}

	/**
	 * @param coursePrice the coursePrice to set
	 */
	public void setCoursePrice(BigDecimal coursePrice) {
		this.coursePrice = coursePrice;
	}

	/**
	 * @return the courseImgUrl
	 */
	public String getCourseImgUrl() {
		return courseImgUrl;
	}

	/**
	 * @param courseImgUrl the courseImgUrl to set
	 */
	public void setCourseImgUrl(String courseImgUrl) {
		this.courseImgUrl = courseImgUrl;
	}

	

	

	/**
	 * @param id
	 * @param courseName
	 * @param courseDescription
	 * @param courseImgUrl
	 * @param coursePrice
	 * @param courseType
	 */
	public CCourse(int id, String courseName, String courseDescription, String courseImgUrl, BigDecimal coursePrice,
			CCourseType courseType) {
		super();
		this.id = id;
		this.courseName = courseName;
		this.courseDescription = courseDescription;
		this.courseImgUrl = courseImgUrl;
		this.coursePrice = coursePrice;
		this.courseType = courseType;
	}

	/**
	 * @return the courseDescription
	 */
	public String getCourseDescription() {
		return courseDescription;
	}

	/**
	 * @param courseDescription the courseDescription to set
	 */
	public void setCourseDescription(String courseDescription) {
		this.courseDescription = courseDescription;
	}

	/**
	 * @return the courseType
	 */
	public CCourseType getCourseType() {
		return courseType;
	}

	/**
	 * @param courseType the courseType to set
	 */
	public void setCourseType(CCourseType courseType) {
		this.courseType = courseType;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the courseName
	 */
	public String getCourseName() {
		return courseName;
	}

	/**
	 * @param courseName the courseName to set
	 */
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	/**
	 * @param id
	 * @param courseName
	 */
	public CCourse(int id, String courseName) {
		super();
		this.id = id;
		this.courseName = courseName;
	}

	/**
	 * 
	 */
	public CCourse() {
		super();
	}
	
	
	
}
