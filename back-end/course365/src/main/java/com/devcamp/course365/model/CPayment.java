package com.devcamp.course365.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="payments")
public class CPayment {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "payment_date")
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date paymentDate;
	
	@ManyToOne
	@JoinColumn(name = "user_id")
	@JsonIgnore
	private User user;
	
	@OneToMany(mappedBy = "payment")
	private List<CEnrollCourse> enrollCourse;
	
	@Column(name = "ammount")
	private BigDecimal ammount;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the paymentDate
	 */
	public Date getPaymentDate() {
		return paymentDate;
	}

	/**
	 * @param paymentDate the paymentDate to set
	 */
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the enrollCourse
	 */
	public List<CEnrollCourse> getEnrollCourse() {
		return enrollCourse;
	}

	/**
	 * @param enrollCourse the enrollCourse to set
	 */
	public void setEnrollCourse(List<CEnrollCourse> enrollCourse) {
		this.enrollCourse = enrollCourse;
	}

	/**
	 * @return the ammount
	 */
	public BigDecimal getAmmount() {
		return ammount;
	}

	/**
	 * @param ammount the ammount to set
	 */
	public void setAmmount(BigDecimal ammount) {
		this.ammount = ammount;
	}

	/**
	 * @param id
	 * @param paymentDate
	 * @param user
	 * @param enrollCourse
	 * @param ammount
	 */
	public CPayment(int id, Date paymentDate, User user, List<CEnrollCourse> enrollCourse, BigDecimal ammount) {
		super();
		this.id = id;
		this.paymentDate = paymentDate;
		this.user = user;
		this.enrollCourse = enrollCourse;
		this.ammount = ammount;
	}

	/**
	 * 
	 */
	public CPayment() {
		super();
	}
	
	
}
