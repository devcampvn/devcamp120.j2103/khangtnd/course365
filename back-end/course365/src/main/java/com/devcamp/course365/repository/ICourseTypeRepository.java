package com.devcamp.course365.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.course365.model.CCourseType;

public interface ICourseTypeRepository extends JpaRepository<CCourseType, Integer>{

}
