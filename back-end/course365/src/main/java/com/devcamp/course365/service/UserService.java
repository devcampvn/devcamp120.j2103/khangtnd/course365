package com.devcamp.course365.service;

import com.devcamp.course365.model.User;
import com.devcamp.course365.security.UserPrincipal;

public interface UserService {
    User createUser(User user);

    UserPrincipal findByUsername(String username);
}
