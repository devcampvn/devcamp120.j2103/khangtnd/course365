package com.devcamp.course365.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="course_types")
public class CCourseType {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "course_type_name")
	private String courseTypeName;
	
	@Column(name = "course_type_description")
	private String courseTypeDescription;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the courseTypeName
	 */
	public String getCourseTypeName() {
		return courseTypeName;
	}

	/**
	 * @param courseTypeName the courseTypeName to set
	 */
	public void setCourseTypeName(String courseTypeName) {
		this.courseTypeName = courseTypeName;
	}

	/**
	 * @return the courseTypeDescription
	 */
	public String getCourseTypeDescription() {
		return courseTypeDescription;
	}

	/**
	 * @param courseTypeDescription the courseTypeDescription to set
	 */
	public void setCourseTypeDescription(String courseTypeDescription) {
		this.courseTypeDescription = courseTypeDescription;
	}

	/**
	 * @param id
	 * @param courseTypeName
	 * @param courseTypeDescription
	 */
	public CCourseType(int id, String courseTypeName, String courseTypeDescription) {
		super();
		this.id = id;
		this.courseTypeName = courseTypeName;
		this.courseTypeDescription = courseTypeDescription;
	}

	/**
	 * 
	 */
	public CCourseType() {
		super();
	}
	
	
}
