package com.devcamp.course365.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import com.devcamp.course365.model.CPayment;
import com.devcamp.course365.model.User;
import com.devcamp.course365.repository.IPayment;
import com.devcamp.course365.repository.UserRepository;

@CrossOrigin
@RestController
public class PaymentController {
	
	@Autowired
	IPayment paymentRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@GetMapping("/payments")
	public List<CPayment> getAllPayment(){
		return paymentRepository.findAll();
	}
	
	@PostMapping("/payment/{userId}")
	public ResponseEntity<Object> createPayment(@PathVariable("userId") Long userId, @RequestBody CPayment cPayment){
		try {
			Optional<User> userFound = userRepository.findById(userId);
			if(userFound.isPresent()) {
				CPayment newPayment = new CPayment();
				newPayment.setAmmount(cPayment.getAmmount());
				newPayment.setPaymentDate(cPayment.getPaymentDate());
				newPayment.setUser(userFound.get());
				
				CPayment createPayment = paymentRepository.save(newPayment);
				return new ResponseEntity<>(createPayment, HttpStatus.OK);
			}else {
				return ResponseEntity.unprocessableEntity().body("Bạn chưa đăng nhập");
			}
		}catch(Exception e) {
			return ResponseEntity.unprocessableEntity().body("Lỗi không tạo được payment");
		}
	}
	
	@DeleteMapping("/payment/{paymentId}")
	public ResponseEntity<Object> deletePayment(@PathVariable("paymentId") Integer paymentId){
		try {
			paymentRepository.deleteById(paymentId);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
	}
}
