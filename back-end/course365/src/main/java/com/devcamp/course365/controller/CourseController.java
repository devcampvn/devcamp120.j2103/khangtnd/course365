package com.devcamp.course365.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.course365.model.CCourse;
import com.devcamp.course365.repository.ICourseRepository;

@CrossOrigin
@RestController
public class CourseController {

	@Autowired
	ICourseRepository courseRepository;
	
	@GetMapping("/courses/all")
	public List<CCourse> getAllCourse() {
		return courseRepository.findAll();
	}
	
	@GetMapping("/courses")
	public ResponseEntity<Map<String, Object>> getAllCoursePagination(
			@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "6") int size) {
		try {
			List<CCourse> vCourses = new ArrayList<CCourse>();
			Pageable paging = PageRequest.of(page, size);
			Page<CCourse> pageCourse;

			pageCourse = courseRepository.findAll(paging);
			vCourses = pageCourse.getContent();
			Map<String, Object> response = new HashMap<>();
			response.put("courses", vCourses);
			response.put("currentPage", pageCourse.getNumber());
			response.put("totalItems", pageCourse.getTotalElements());
			response.put("totalPages", pageCourse.getTotalPages());
			return new ResponseEntity<>(response, HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	@GetMapping("/course/filter")
	public ResponseEntity<Map<String, Object>> getCourseByCourseType(
			@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "6") int size,
			@RequestParam("type") int courseTypeId) {
		try {
			List<CCourse> vCourses = new ArrayList<CCourse>();
			Pageable paging = PageRequest.of(page, size);
			Page<CCourse> pageCourse;

			pageCourse = courseRepository.findByCourseTypeId(courseTypeId,paging);
			vCourses = pageCourse.getContent();
			Map<String, Object> response = new HashMap<>();
			response.put("courses", vCourses);
			response.put("currentPage", pageCourse.getNumber());
			response.put("totalItems", pageCourse.getTotalElements());
			response.put("totalPages", pageCourse.getTotalPages());
			return new ResponseEntity<>(response, HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	/*
	 * get course by id
	 * @param course Id
	 */
	@GetMapping("/course")
	public ResponseEntity<CCourse> getCourseById(@RequestParam("id") Integer courseId){
		try {
			Optional<CCourse> course = courseRepository.findById(courseId);
			return new ResponseEntity<>(course.get(), HttpStatus.OK);
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}
	
}
