package com.devcamp.course365.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.course365.model.CCourse;
import com.devcamp.course365.model.CEnrollCourse;

public interface ICourseRepository extends JpaRepository<CCourse, Integer>{

	Page<CCourse> findAll(Pageable paging);
	
	@Query(value="SELECT * FROM courses WHERE course_type_id LIKE :courseTypeId", nativeQuery = true)
	Page<CCourse> findByCourseTypeId(@Param("courseTypeId") Integer courseTypeId, Pageable paging);
}
