package com.devcamp.course365.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.Nullable;

@Entity
@Table(name="enroll_courses")
public class CEnrollCourse {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@OneToOne
	@JoinColumn(name = "user_id")
	private User userId;
	
	@OneToOne
	@JoinColumn(name = "course_id")
	private CCourse courseId;
	
	@Column(name = "course_status")
	private String courseStatus;
	
	@OneToOne
	@JoinColumn(name = "lesson_id")
	@Nullable
	private CLesson lessonId;
	
	@ManyToOne
	@JoinColumn(name = "payment_id")
	@JsonIgnore
	private CPayment payment;

	

	/**
	 * @return the payment
	 */
	public CPayment getPayment() {
		return payment;
	}

	/**
	 * @param payment the payment to set
	 */
	public void setPayment(CPayment payment) {
		this.payment = payment;
	}

	/**
	 * @param id
	 * @param userId
	 * @param courseId
	 * @param courseStatus
	 * @param lessonId
	 * @param payment
	 */
	public CEnrollCourse(int id, User userId, CCourse courseId, String courseStatus, CLesson lessonId,
			CPayment payment) {
		super();
		this.id = id;
		this.userId = userId;
		this.courseId = courseId;
		this.courseStatus = courseStatus;
		this.lessonId = lessonId;
		this.payment = payment;
	}

	/**
	 * 
	 */
	public CEnrollCourse() {
		super();
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the userId
	 */
	public User getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(User userId) {
		this.userId = userId;
	}

	/**
	 * @return the courseId
	 */
	public CCourse getCourseId() {
		return courseId;
	}

	/**
	 * @param courseId the courseId to set
	 */
	public void setCourseId(CCourse courseId) {
		this.courseId = courseId;
	}

	/**
	 * @return the courseStatus
	 */
	public String getCourseStatus() {
		return courseStatus;
	}

	/**
	 * @param courseStatus the courseStatus to set
	 */
	public void setCourseStatus(String courseStatus) {
		this.courseStatus = courseStatus;
	}

	/**
	 * @return the lessonId
	 */
	public CLesson getLessonId() {
		return lessonId;
	}

	/**
	 * @param lessonId the lessonId to set
	 */
	public void setLessonId(CLesson lessonId) {
		this.lessonId = lessonId;
	}
	
	
	
}
