package com.devcamp.course365.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.course365.model.CEnrollCourse;

public interface IEnrollCourse extends JpaRepository<CEnrollCourse, Integer>{
	
	@Query(value="SELECT * FROM enroll_courses WHERE user_id LIKE :userId ", nativeQuery = true)
	List<CEnrollCourse> findAllByUserId(@Param("userId") Long userId);
	
	@Query(value="INSERT INTO enroll_courses(id, course_status, course_id, lesson_id, user_id, payment_id)\r\n"
			+ "VALUES (NULL,:courseStatus,:courseId, NULL, :userId,:paymentId)", nativeQuery = true)
	List<CEnrollCourse> addEnrollCourse(
			@Param("courseStatus") String courseStatus,
			@Param("courseId") Integer courseId,
			@Param("userId") Long userId,
			@Param("paymentId") Integer paymentId);
	
	@Query(value="INSERT INTO enroll_courses(course_status, course_id) VALUES (:courseStatus,:courseId)", nativeQuery = true)
	List<CEnrollCourse> addEnrollCourseTest(
			@Param("courseStatus") String courseStatus,
			@Param("courseId") Integer courseId);
	
	
}
