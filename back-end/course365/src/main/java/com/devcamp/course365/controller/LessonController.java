package com.devcamp.course365.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.course365.model.CLesson;
import com.devcamp.course365.repository.ILessonRepository;

@CrossOrigin
@RestController
public class LessonController {
	
	@Autowired
	ILessonRepository lessonRepository;
	
	@GetMapping("/lesson/all")
	public List<CLesson> getAllLesson() {
		return lessonRepository.findAll();
	}
	
	
	@GetMapping("/lesson")
	public ResponseEntity<Object> findLessonByCourseId(@RequestParam("courseId") Integer courseId){
		try {
			return new ResponseEntity<>(lessonRepository.findLessonByCourseId(courseId), HttpStatus.OK);
		}catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
