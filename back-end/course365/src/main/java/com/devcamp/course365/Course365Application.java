package com.devcamp.course365;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Course365Application {

	public static void main(String[] args) {
		SpringApplication.run(Course365Application.class, args);
	}

}
