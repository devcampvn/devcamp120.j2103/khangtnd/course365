package com.devcamp.course365.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.course365.model.CLesson;

public interface ILessonRepository extends JpaRepository<CLesson, Integer>{
	
	@Query(value="SELECT * FROM lessons WHERE course_id LIKE :courseId ORDER BY lesson_index ASC", nativeQuery = true)
	List<CLesson> findLessonByCourseId(@Param("courseId") Integer courseId);
}
