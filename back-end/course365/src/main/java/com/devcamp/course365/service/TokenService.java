package com.devcamp.course365.service;

import com.devcamp.course365.model.Token;

public interface TokenService {

    Token createToken(Token token);

    Token findByToken(String token);
}
