package com.devcamp.course365.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.course365.model.CPayment;

public interface IPayment extends JpaRepository<CPayment, Integer>{

}
