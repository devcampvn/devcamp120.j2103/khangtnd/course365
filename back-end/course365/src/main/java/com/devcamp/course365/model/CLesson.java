package com.devcamp.course365.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="lessons")
public class CLesson {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "lesson_index")
	private Integer lessonIndex;
	
	@Column(name = "lesson_name")
	private String lessonName;
	
	@Column(name = "lesson_content", length = 500)
	private String lessonContent;
	
	@ManyToOne
	@JoinColumn(name ="course_id")
	@JsonIgnore
	private CCourse course;

	

	/**
	 * @param id
	 * @param lessonIndex
	 * @param lessonName
	 * @param lessonContent
	 * @param course
	 */
	public CLesson(int id, Integer lessonIndex, String lessonName, String lessonContent, CCourse course) {
		super();
		this.id = id;
		this.lessonIndex = lessonIndex;
		this.lessonName = lessonName;
		this.lessonContent = lessonContent;
		this.course = course;
	}
	
	
	
	/**
	 * @return the lessonIndex
	 */
	public Integer getLessonIndex() {
		return lessonIndex;
	}



	/**
	 * @param lessonIndex the lessonIndex to set
	 */
	public void setLessonIndex(Integer lessonIndex) {
		this.lessonIndex = lessonIndex;
	}



	/**
	 * 
	 */
	public CLesson() {
		super();
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the lessonName
	 */
	public String getLessonName() {
		return lessonName;
	}

	/**
	 * @param lessonName the lessonName to set
	 */
	public void setLessonName(String lessonName) {
		this.lessonName = lessonName;
	}

	/**
	 * @return the lessonContent
	 */
	public String getLessonContent() {
		return lessonContent;
	}

	/**
	 * @param lessonContent the lessonContent to set
	 */
	public void setLessonContent(String lessonContent) {
		this.lessonContent = lessonContent;
	}

	/**
	 * @return the course
	 */
	public CCourse getCourse() {
		return course;
	}

	/**
	 * @param course the course to set
	 */
	public void setCourse(CCourse course) {
		this.course = course;
	}
	
	
}
